output "instance_public_ips" {
  value       = aws_instance.instances[*].public_ip
  description = "Public IP addresses of the instances"
}

output "eip_allocation_ids" {
  value       = aws_eip.eips[*].id
  description = "Elastic IP allocation IDs"
}