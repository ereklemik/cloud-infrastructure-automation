provider "aws" {
  region = var.aws_region
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"]
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_vpc" "my_vpc" {
  cidr_block = var.vpc_cidr
}

resource "aws_internet_gateway" "my_igw" {
  vpc_id = aws_vpc.my_vpc.id
}

resource "aws_subnet" "subnets" {
  count             = length(var.subnet_cidrs)
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = element(var.subnet_cidrs, count.index)
  availability_zone = element(var.availability_zones, count.index)
}

resource "aws_route_table" "route_tables" {
  count  = length(var.subnet_cidrs)
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_igw.id
  }

  tags = {
    Name = "${var.environments[count.index]}-route-table"
  }
}

resource "aws_route_table_association" "route_table_associations" {
  count          = length(var.subnet_cidrs)
  subnet_id      = aws_subnet.subnets[count.index].id
  route_table_id = aws_route_table.route_tables[count.index].id
}

resource "aws_security_group" "my_sg" {
  vpc_id = aws_vpc.my_vpc.id

  dynamic "ingress" {
    for_each = [22, 80, 443]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic "egress" {
    for_each = [22, 80, 443]
    content {
      from_port   = egress.value
      to_port     = egress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}

resource "aws_instance" "instances" {
  count                       = length(var.environments)
  subnet_id                   = aws_subnet.subnets[count.index].id
  instance_type               = var.instance_type
  ami                         = data.aws_ami.ubuntu.id
  key_name                    = var.ssh_key_name
  vpc_security_group_ids      = [aws_security_group.my_sg.id]
  associate_public_ip_address = true

  tags = {
    Name = "${var.environments[count.index]}-instance"

  }
}
resource "aws_eip" "eips" {
  count    = length(var.subnet_cidrs)
  instance = aws_instance.instances[count.index].id
  vpc      = true
}

resource "aws_eip_association" "eip_assoc" {
  count         = length(var.subnet_cidrs)
  allocation_id = aws_eip.eips[count.index].id
  instance_id   = aws_instance.instances[count.index].id
}


resource "tls_private_key" "my_ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "my_key_pair" {
  key_name   = var.ssh_key_name
  public_key = tls_private_key.my_ssh_key.public_key_openssh
}






